# etherbay
Decentralized ecommerce of the future

## Developing

- Run an instance of testrpc
- Login to local blockchain using metamask
- `truffle migrate --reset`
- `npm run dev`

## Testing

- `truffle test`