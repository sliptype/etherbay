import normalize from 'normalize.css'
import '../styles/main.scss'
import '../styles/card.scss'
import '../styles/button.scss'

import Vue from 'vue'
import Vuex from 'vuex'
import Resource from 'vue-resource'
import Router from 'vue-router'
import Routes from './plugins/Routes'
import Loader from './plugins/Loader'

import App from './components/App.vue'
import Search from './components/Search.vue'
import List from './components/List.vue'
import Profile from './components/Profile.vue'
import ListingForm from './components/ListingForm.vue'

// Install plugins
Vue.use(Router)
Vue.use(Routes)
Vue.use(Resource)
Vue.use(Vuex)
Vue.use(Loader)

// route config
let routes = [
  {
    path: '/home',
    name: 'home',
    component: List,
    props: { config: { type: 'Listing', display: 'Tile' } }
  },
  {
    path: '/search/:queryString',
    name: 'search',
    component: List,
    props: { config: { type: 'Listing', display: 'Tile' } }
  },
  {
    path: '/listing/:id',
    name: 'listing',
    component: List,
    props: { config: { type: 'Listing', display: 'Expanded' } }
  },
  {
    path: '/user/:address',
    name: 'user',
    component: Profile
  },
  {
    path: '/post',
    name: 'post',
    component: ListingForm
  },
  {
    path: '/purchases',
    name: 'purchases',
    component: List,
    props: { config: { type: 'Order' } }
  },
  {
    path: '/sales',
    name: 'sales',
    component: List,
    props: { config: { type: 'Order' } }
  },

  { path: '*', redirect: '/home' }
]

// Set up a new router
let router = new Router({
  routes: routes
})

const store = new Vuex.Store({
  state: {
    account: null
  },
  mutations: {
    UPDATE_ACCOUNT (state, account) {
      state.account = account
    }
  }
})

// Start up our app
new Vue({
  router: router,
  render: h => h(App),
  store
}).$mount('#app')

