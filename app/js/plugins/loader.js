import { EventBus } from '../eventbus.js';
import Chain from '../services/Chain.js';
let timer;

const Loader = {
  install(Vue, options) {
    Vue.mixin({
      data() {
        return {
          loading: false
        }
      },
      methods: {
        startLoad(instant) {
          if (instant) {
            this.loading = true;
          } else {
            timer = setTimeout(() => {
              this.loading = true;
            }, 250);
          }
        },
        stopLoad(redraw) {
          clearTimeout(timer);

          if (redraw) {
            this.redraw();
            // EventBus.$emit('redraw');
          }

          this.loading = false;

          if (this.afterLoad) {
            this.afterLoad();
          }
        },

        // Returns a function that wraps an action
        loadAction(action, redraw, contract, event, callback) {
          let _this = this;
          return async function () {
            _this.startLoad();

            // Wait for blockchain success event 
            if (event) {
              let callbackWrap = () => {
                if (callback) {
                  callback();
                }
                _this.stopLoad(redraw);
              }

              // TODO: clear out this listener (dups??)
              Chain.watch(contract, event, callbackWrap);
            }

            await action();

            if (!event) {
              _this.stopLoad(redraw);
            }
          }
        }
      }
    });
  }
};

export default Loader;