import RoutesService from '../services/Routes.js' 

const Routes = {
  install(Vue, options) {
    Vue.mixin({
      data() {
        return {
          routes: null
        }
      },
      mounted() {
        this.routes = RoutesService(this.$router);
      }
    });
  }
};

export default Routes;