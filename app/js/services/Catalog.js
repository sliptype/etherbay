import { default as contract } from 'truffle-contract';
import catalogArtifacts from '../../../build/contracts/CatalogMock.json';
import Chain from '../services/Chain';
import Directory from '../services/Directory';
let _account;

export default {

  init: async function (provider, account, address) {
    let catalog = contract(catalogArtifacts);
    catalog.setProvider(provider);
    this.deployed = await catalog.at(address);
    _account = account;
  },

  estimate: async function (title, description, tags, price) {
    return await Chain.estimateGas(this.deployed, 'add', [title, description, tags, price]);
  },

  add: async function (title, description, tags, price) {
    await this.deployed.add(title, description, tags, price, {from: _account});
  },

  get: async function (id) {
    let result = await this.deployed.get.call(id);
    let identity = await Directory.deployed.get.call(result[0]);

    let mappedResult = {
      id: id,
      owner: result[0],
      title: web3.toAscii(result[1]),
      description: web3.toAscii(result[2]),
      price: web3.fromWei(result[3], 'ether'),
      rating: result[4].toNumber(),
      reviewCount: result[5].toNumber(),
      identity: await Directory.get(result[0])
    };

    return mappedResult;
  },

  getTitle: async function (id) {
    let result = await this.deployed.get.call(id);
    return web3.toAscii(result[1]);
  },

  getReviews: async function (id) {
    let reviews = await this.deployed.getReviews(id);
    let _this = this;
    let mappedResult = [];

    for (let r in reviews) {
      mappedResult.push(await this.getReview(reviews[r]));
    }

    return mappedResult;
  },

  getUserReviews: async function (address, received) {
    let reviews = received ? await this.deployed.getVendorReviews(address) : await this.deployed.getCustomerReviews(address);
    let mappedResult = [];

    for (let r in reviews) {
      mappedResult.push(await this.getReview(reviews[r]));
    }

    return mappedResult;
  },

  getReview: async function(id) {
    let result = await this.deployed.getReview(id);

    let mappedResult = {
      customer: result[0],
      satisfied: result[1],
      text: result[2],
      dateIso: result[3],
      listingId: result[4] 
    }

    return mappedResult;
  },

  getFromVendor: async function (vendor) {
    let result = await this.deployed.getFromVendor.call(vendor);
    let mappedResult = [];

    for (let i = 0; i < result.length; i++) {
      mappedResult.unshift(await this.get(result[i]));
    }

    return mappedResult;
  },

  search: async function (query) {
    let result = await this.deployed.search.call(query);

    console.log(result);

    let quantity = result[0].toNumber();
    let id = result[1];
    let mappedResult = [];

    for (let i = 0; i < quantity; i++) {
      mappedResult.unshift(await this.get(id[i]));
    }

    return mappedResult;
  },
  
  deployed: null

}