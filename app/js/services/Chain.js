import { default as Web3 } from 'web3';
import { EventBus } from '../eventbus.js'
let accounts;

export default {
  init: function () {

    return new Promise((resolve, reject) => {

      if (typeof web3 !== 'undefined') {
        // Use Mist/MetaMask's provider
        window.web3 = new Web3(web3.currentProvider);
      } else {
        console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
        // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
        window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
      }

      web3.eth.getAccounts(function(err, accs) {
        if (err != null) {
          alert("There was an error fetching your accounts.");
          return;
        }

        if (accs.length == 0) {
          alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
          return;
        }

        accounts = accs;
        web3.eth.defaultAccount = accounts[0];

        resolve(web3.eth.defaultAccount);

      });

    });

  },

  getProvider: () => web3.currentProvider,

  estimateGas: async function (contract, method, data) {
    if (contract[method]) {
      return await contract[method].estimateGas(...data);
    }
    throw new Error('Method does not exist');
  },

  getMaxGasLimit: async function () {
    return new Promise((resolve, reject) => {
      web3.eth.getBlock('latest', function (error, result) {
        resolve(result.gasLimit);
      });
    });
  },
  
  afterDeploy(contract, callback) {
    // Check that contract is deployed
    if (contract.deployed) {
      callback();
    } else {
      EventBus.$on('web3initialized', callback);
    }
  },

  watch(contract, event, callback) {
    let time = Math.floor((new Date).getTime()/1000);
    this.afterDeploy(contract, () => {
      contract.deployed[event]({ addr: web3.eth.defaultAccount}, (error, result) => {
        if (result.args.timestamp >= time) {
          callback(result);
        }
      });
    });
  }
}
