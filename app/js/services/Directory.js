import { default as contract } from 'truffle-contract';
import directoryArtifacts from '../../../build/contracts/Directory.json';
let _account;

export default {

  init: async function (provider, account, address) {
    let directory = contract(directoryArtifacts);
    directory.setProvider(provider);
    this.deployed = await directory.at(address);
    _account = account;
  },

  add: async function (alias) {
    await this.deployed.add(web3.toHex(alias), {from: _account});
  },

  exists: async function (alias) {
    return await this.deployed.aliases.call(web3.toHex(alias));
  },

  get: async function (address) {
    let result = await this.deployed.get.call(address);

    let mappedResult = {
      alias: web3.toAscii(result[0]).replace(/\0/g, ''),
      vendorRep: result[1],
      customerRep: result[2]
    };

    return mappedResult;
  },
  
  deployed: null

}