import { default as contract } from 'truffle-contract';
import ledgerArtifacts from '../../../build/contracts/Ledger.json';
import Order from '../services/Order';
let _account;

export default {

  init: async function (provider, account, address) {
    let ledger = contract(ledgerArtifacts);
    ledger.setProvider(provider);
    this.deployed = await ledger.at(address);
    _account = account;
  },

  placeOrder: async function (listingId, value) {
    return await this.deployed.placeOrder(listingId, {from: _account, value });
  },

  get: async function (orders) {
    let mappedResult = [];

    for (var i = 0; i < orders.length; i++) {
      let order = await Order(orders[i], _account);
      mappedResult.unshift(await order.get()); 
    }

    console.log(mappedResult);

    return mappedResult;
  },

  getPurchases: async function () {
    let purchases = await this.deployed.getPurchases.call(_account);
    return await this.get(purchases);
  },

  getSales: async function () {
    let sales = await this.deployed.getSales.call(_account);
    return await this.get(sales);
  },

  deployed: null

}