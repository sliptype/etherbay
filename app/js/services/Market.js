import { default as contract } from 'truffle-contract';
let mocks = require('../utils/mocks').default();
let marketArtifacts = require(`../../../build/contracts/${mocks.market.artifacts}`);
let _account;

export default {

  init: async function (provider, account) {
    let market = contract(marketArtifacts);
    market.setProvider(provider);

    this.deployed = await market.deployed();
    _account = account;
  },

  getCatalog: async function () {
    return await this.deployed.catalog.call();
  },

  getLedger: async function () {
    return await this.deployed.ledger.call();
  },

  getDirectory: async function () {
    return await this.deployed.directory.call();
  },

  deployed: null
}