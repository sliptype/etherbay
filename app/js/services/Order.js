import { default as contract } from 'truffle-contract';
import orderArtifacts from '../../../build/contracts/Order.json';
import Chain from '../services/Chain';
import Catalog from '../services/Catalog';

export default async function (address, account) {

  let order = contract(orderArtifacts);
  order.setProvider(Chain.getProvider());
  let deployed = await order.at(address);

  const get = async function () {
    let customer = await deployed.customer.call();
    let vendor = await deployed.vendor.call();
    let price = web3.fromWei(await deployed.price.call(), 'ether');
    let stateId = await deployed.state.call();
    let dateIso = parseInt(await deployed.date.call());
    let shipped = await deployed.shipped.call();
    let reviewed = await deployed.reviewed.call();
    let listingId = await deployed.listingId.call();
    let title = await Catalog.getTitle(listingId);
    let balance = await deployed.getBalance.call();

    return {
      address,
      customer,
      vendor,
      price,
      stateId,
      dateIso,
      shipped,
      reviewed,
      balance,
      listingId,
      title,
      instance
    };
  };

  const finalize = async function () {
    return await deployed.finalize({from: account});
  };

  const refund = async function () {
    return await deployed.refund({from: account});
  };

  const withdraw = async function () {
    return await deployed.withdraw({from: account});
  };

  const review = async function (satisfied, text) {
    return await deployed.review(satisfied, text, {from: account});
  };

  const instance = {
    get,
    finalize,
    refund,
    withdraw,
    review
  };

  return instance;
}