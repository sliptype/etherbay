export default function (router) {

  const viewAccount = (address) => {
    router.push({ name: 'user', params: { address }});
  }

  const viewListing = (id) => {
    router.push({ name: 'listing', params: { id }});
  }

  const viewPurchases = () => {
    router.push({ name: 'purchases' });
  }

  const search = (queryString) => {
    router.push({ name: 'search', params: { queryString }});
  }

  return {
    viewAccount,
    viewListing,
    viewPurchases,
    search
  }

}