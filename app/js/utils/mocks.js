function contractFiles(code, artifacts) {
  return {
    code,
    artifacts
  }
};

export default function () {
  let mocks = {};

  if (process.env.mock || MOCK) {
    mocks.market = contractFiles('MarketMock.sol', 'MarketMock.json');
  } else {
    mocks.market = contractFiles('Market.sol', 'Market.json');
  }

  return mocks;
}