pragma solidity ^0.4.4;

import "./Ledger.sol";
import "./Directory.sol";

contract Catalog {

  struct Listing {
    address vendor;
    string title;
    string description;
    uint256 price;
    bool available;
    bool exists;
    uint rating;
    uint reviewCount;
  }

  struct Review {
    address customer;
    string text;
    bool satisfied;
    uint date;
    bytes32 listingId;
  }

  event ListingPosted(address indexed addr, bytes32 id, uint timestamp);

  mapping(bytes32 => Listing) public listings;
  mapping(bytes32 => bytes32[]) public listingReviews;
  mapping(bytes32 => Review) reviews;
  mapping(address => bytes32[]) vendorReviews;
  mapping(address => bytes32[]) customerReviews;
  mapping(address => bytes32[]) public vendors;
  mapping(bytes32 => bytes32[]) index;
  bytes32[] searchResults;

  Ledger ledger;
  Directory directory;

  function add(string _title, string _description, bytes32[] _tags, uint256 _price) {

    bytes32 id = sha3(msg.sender, _title);

    require(!listings[id].exists);

    listings[id] = Listing(msg.sender, _title, _description, _price, true, true, 0, 0);

    vendors[msg.sender].push(id);
    for (uint i = 0; i < _tags.length; i++) {
      bytes32 tag = _tags[i];
      index[tag].push(id);
    }

    ListingPosted(msg.sender, id, now);
  }

  function get(bytes32 id) constant returns (address _vendor, string _title, string _description, uint256 _price, uint _rating, uint _reviewCount) {
    Listing listing = listings[id];
    require(listing.exists);

    _vendor = listing.vendor; 
    _title = listing.title;
    _description = listing.description;
    _price = listing.price;
    _rating = listing.rating;
    _reviewCount = listing.reviewCount;
  }

  function getFromVendor(address vendor) constant returns (bytes32[] _id) {
    _id = vendors[vendor];
  }

  function search(bytes32[] tags) constant returns (uint _quantity, bytes32[] _id) {

    _quantity = 0;

    for (uint i = 0; i < tags.length; i++) {
      bytes32 tag = tags[i];
      if (index[tag].length > 0) {
        for (uint j = 0; j < index[tag].length; j++) {
          bytes32 id = index[tag][j];
          Listing listing = listings[id];

          if (listing.available) {
            searchResults.push(id);
            _quantity = _quantity + 1;

            // prevent duplicate results
            listing.exists = false;
          }
        }
      }
    }

    _id = searchResults;
  }

  function reviewListing(address customer, bytes32 listingId, bool satisfied, string text) {
    require(msg.sender == address(ledger));
    require(listings[listingId].exists);
    // Listing listing = listings[id];

    bytes32 id = sha3(msg.sender, text);

    reviews[id] = Review(customer, text, satisfied, now, listingId);
    listingReviews[listingId].push(id);
    vendorReviews[listings[listingId].vendor].push(id);
    customerReviews[customer].push(id);

    uint rating = satisfied ? 100 : 0;

    listings[listingId].reviewCount = listingReviews[listingId].length;
    listings[listingId].rating = ((listings[listingId].rating * (listings[listingId].reviewCount - 1)) + rating) / listings[listingId].reviewCount;

    int rep = satisfied ? int(10) : -10;
    directory.updateVendorRep(listings[listingId].vendor, rep);
    directory.updateCustomerRep(customer, rep);
  }

  function getReviews(bytes32 id) constant returns(bytes32[]) {
    return listingReviews[id];
  }

  function getReview(bytes32 id) constant returns(address customer, bool satisfied, string text, uint date, bytes32 listingId) {
    return (reviews[id].customer, reviews[id].satisfied, reviews[id].text, reviews[id].date, reviews[id].listingId);
  }

  function getVendorReviews(address vendor) constant returns(bytes32[]) {
    return vendorReviews[vendor];
  }

  function getCustomerReviews(address customer) constant returns(bytes32[]) {
    return customerReviews[customer];
  }

  function setLedger(Ledger _ledger) {
    require(ledger == address(0));
    ledger = _ledger;
  }

  function setDirectory(Directory _directory) {
    require(directory == address(0));
    directory = _directory;
  }
}

