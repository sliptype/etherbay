pragma solidity ^0.4.4;

import "./Catalog.sol";

contract CatalogMock is Catalog {

  bytes32 public listingId = 0x0000000000000000000000000000000000000000000000000000000000000000;

  string title = '0x74657374';
  bytes32 tag = 0x7465737400000000000000000000000000000000000000000000000000000000;
  bytes32[] tags;
  string description = '0x0';
  uint256 price = 2000000000000000000;
  bool available = true;
  bool exists = true;

  function CatalogMock() {
    tags.push(tag);
    add(title, description, tags, price);
  }
}