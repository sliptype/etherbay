pragma solidity ^0.4.4;

import "./Catalog.sol";

contract Directory {

  struct Identity {
    bytes32 alias;
    int vendorRep;
    int customerRep;
    bool exists;
  }

  event AliasUpdated(address indexed addr, uint timestamp);

  mapping(address => Identity) public directory;
  mapping(bytes32 => bool) public aliases;
  Catalog catalog;

  function Directory(Catalog _catalog) {
    catalog = _catalog;
  }

  function add(bytes32 _alias) {
    require(!directory[msg.sender].exists);
    require(!aliases[_alias]);
    directory[msg.sender] = Identity(_alias, 0, 0, true);
    aliases[_alias] = true;
    AliasUpdated(msg.sender, now);
  }

  function get(address identity) constant returns (bytes32 alias, int vendorRep, int customerRep) {
    return (directory[identity].alias, directory[identity].vendorRep, directory[identity].customerRep);
  }

  function updateVendorRep(address addr, int amount) {
    require(msg.sender == address(catalog));
    directory[addr].vendorRep += amount;
  }

  function updateCustomerRep(address addr, int amount) {
    require(msg.sender == address(catalog));
    directory[addr].customerRep += amount;
  }

}