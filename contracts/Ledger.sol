pragma solidity ^0.4.4;

import "./Order.sol";
import "./Catalog.sol";

contract Ledger {
  mapping (address => Order[]) public purchases;
  mapping (address => Order[]) public sales;
  mapping (bytes32 => Order[]) public listingOrders;

  Catalog catalog;

  event OrderPlaced(address indexed addr, uint timestamp);

  function Ledger(Catalog _catalog) {
    catalog = _catalog;
  }

  function placeOrder(bytes32 id) payable returns (Order) {
    var (vendor, , , price, available, ) = catalog.listings(id);

    if (available) {

      if (msg.value >= price) {

        uint orderId = purchases[msg.sender].length;

        Order orderAddress = new Order(msg.sender, vendor, price, id, orderId, this);

        orderAddress.transfer(price);
        msg.sender.transfer(msg.value - price);

        purchases[msg.sender].push(orderAddress);
        sales[vendor].push(orderAddress);
        listingOrders[id].push(orderAddress);

        OrderPlaced(msg.sender, now);

        return orderAddress;
      }
    }
  }

  function reviewOrder(address customer, uint orderId, bool satisfied, string text) {
    require(purchases[customer][orderId] == msg.sender);
    catalog.reviewListing(customer, purchases[customer][orderId].listingId(), satisfied, text);
  }

  function getBalance() constant returns (uint) {
    return this.balance;
  }

  function getPurchases(address customer) constant returns(Order[]) {
    return purchases[customer];
  }

  function getSales(address vendor) constant returns(Order[]) {
    return sales[vendor];
  }
}