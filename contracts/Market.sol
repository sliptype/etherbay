pragma solidity ^0.4.4;

import "./Catalog.sol";
import "./Ledger.sol";
import "./Directory.sol";

contract Market {

  Catalog public catalog;
  Ledger public ledger; 
  Directory public directory;

  function initCatalog() {
    catalog = new Catalog();
  }

  function initLedger() {
    ledger = new Ledger(catalog);
    catalog.setLedger(ledger);
  }

  function initDirectory() {
    directory = new Directory(catalog);
    catalog.setDirectory(directory);
  }

}