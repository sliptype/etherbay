pragma solidity ^0.4.4;

import "./Market.sol";
import "./CatalogMock.sol";
import "./Ledger.sol";

contract MarketMock is Market {

  function initCatalog() {
    catalog = new CatalogMock();
  }

}