pragma solidity ^0.4.4;

import "./Ledger.sol";

contract Order {

  enum State { Created, Canceled, Finalized, Refunded }
  State public state;

  address public customer;
  address public vendor;
  uint256 public price;
  bytes32 public listingId;
  uint orderId;
  uint public date;
  bool public shipped;
  bool public reviewed;
  Ledger ledger;

  function Order(address _customer, address _vendor, uint256 _price, bytes32 _listingId, uint _orderId, Ledger _ledger) {
    customer = _customer;
    vendor = _vendor;
    price = _price;
    listingId = _listingId;
    orderId = _orderId;
    date = now;
    shipped = false;
    reviewed = false;
    ledger = _ledger;

    state = State.Created;
  }

  function ship() 
    onlyBy(vendor)
  {
    if (state == State.Created || state == State.Finalized) {
      shipped = true;
    }
  }

  function finalize() 
    onlyBy(customer)
    inState(State.Created)
  {
    state = State.Finalized;
    // TODO: Fire an event
  }

  function refund() 
    onlyBy(vendor)
    inState(State.Finalized)
  {
    state = State.Refunded;
  }

  function withdraw() {
    if (msg.sender == customer && state != State.Finalized) {
      state = State.Canceled;
      customer.transfer(this.balance);
    } else if (msg.sender == vendor && state == State.Finalized) {
      vendor.transfer(this.balance);
    } 
  }

  function review(bool satisfied, string text) 
    onlyBy(customer)
    inState(State.Finalized)
  {
    require(!reviewed);
    reviewed = true;
    ledger.reviewOrder(msg.sender, orderId, satisfied, text);
  }
  
  function getBalance() constant returns (uint) {
    return this.balance;
  }

  function () payable { }

  modifier onlyBy(address _account)
  {
    require(msg.sender == _account);
    _;
  }

  modifier inState(State _state) {
    require(state == _state);
    _;
  }

}