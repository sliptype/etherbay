let mocks = require('../app/js/utils/mocks').default();
let Market = artifacts.require(mocks.market.code);

module.exports = async function(deployer) {
  await deployer.deploy(Market);
  let market = await Market.deployed();
  await market.initCatalog();
  await market.initLedger();
  await market.initDirectory();
};
