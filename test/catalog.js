var Catalog = artifacts.require('Catalog');
var Market = artifacts.require('Market');
import listing from './data/listing.js';

const catalogInstance = async function () {
  let market = await Market.deployed();
  let catalogAddress = await market.catalog();
  return await Catalog.at(catalogAddress);
}

contract('Catalog', function(accounts) {

  let catalog;
  let id;

  it('should accept new listings', async function() {
    catalog = await catalogInstance();
    let result = await catalog.add(listing.title, listing.description, listing.tags, listing.price);
  });

  it('should return listings from vendor', async function() {
    let result = await catalog.getFromVendor.call(accounts[0]);
    assert.lengthOf(result, 1);
    id = result[0];
  });

  it('should return listing from id', async function() {
    let result = await catalog.get.call(id);
    assert.typeOf(result[0], 'string');
    assert.typeOf(result[1], 'string');
    assert.typeOf(result[2], 'string');
    assert.typeOf(result[3], 'object');
  });

  it('should return search results', async function() {
    let result = await catalog.search.call(listing.tags);
    assert.lengthOf(result[1], 1);
  });

  //TODO : No duplicate search results

  //TODO : No duplicate listings

});