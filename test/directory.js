var Directory = artifacts.require('Directory');
var Market = artifacts.require('Market');
import user from './data/user.js';

contract('Directory', function(accounts) {

  let directory;
  let amount = 10;

  before(async function() {
    let market = await Market.deployed();
    let directoryAddress = await market.directory();
    directory = await Directory.at(directoryAddress);
    await directory.create(user.alias, {from: accounts[0]});
  });

  it('should accept new identities', async function() {
    let result = await directory.directory.call(accounts[0]);

    assert.equal(result[0], user.alias);
  });

  it('should increase reputation for a given address', async function() {
    let before = (await directory.directory.call(accounts[0]))[1];
    await directory.increase(accounts[0], amount, {from: accounts[0]});
    let after = (await directory.directory.call(accounts[0]))[1];

    assert(before.plus(amount).equals(after));
  });

  it('should decrease reputation for a given address', async function() {
    let before = (await directory.directory.call(accounts[0]))[1];
    await directory.decrease(accounts[0], amount, {from: accounts[0]});
    let after = (await directory.directory.call(accounts[0]))[1];

    assert(before.minus(amount).equals(after));
  });
});