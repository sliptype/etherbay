var Ledger = artifacts.require('Ledger');
var Order = artifacts.require('Order');
var MarketMock = artifacts.require('MarketMock');
import listing from './data/listing.js';

const ledgerInstance = async function () {
  let market = await MarketMock.new();
  let ledgerAddress = await market.ledger();
  return await Ledger.at(ledgerAddress);
}

contract('Ledger', function (accounts) {

  let ledger;
  let orderTx;
  let orders;
  let order;

  before(async function () {
    ledger = await ledgerInstance();
    orderTx = await ledger.placeOrder(listing.id, {value: listing.price});
    orders = await ledger.getPurchases.call(accounts[0]);
    order = await Order.at(orders[0]);
  });

  it('should initialize an order', async function () {
    assert.typeOf(orderTx, 'object');
  });

  it('should fund new orders', async function () {
    let orderBalance = await order.getBalance.call();
    assert.equal(orderBalance, listing.price);
  });

  it('should return customer orders', async function () {
    assert.lengthOf(orders, 1);
  });
});