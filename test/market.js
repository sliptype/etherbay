var Market = artifacts.require('Market');

contract('Market', function(accounts) {

  let market;

  before(async function() {
    market = await Market.deployed();
  });

  it('should initialize a catalog', async function() {
    let result = await market.catalog();
    assert.typeOf(result, 'string');
  });

  it('should initialize a ledger', async function() {
    let result = await market.ledger();
    assert.typeOf(result, 'string');
  });
});