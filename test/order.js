var Order = artifacts.require('Order');
import BigNumber from 'bignumber.js';
import listing from './data/listing.js';
import balance from './util/balance.js';
import throwsError from './util/error.js';

contract('Order', function(accounts) {

  let order;
  let states = ['Created', 'Canceled', 'Finalized', 'Refunded'];
  let customer = accounts[0];
  let vendor = accounts[1];
  let threshold = 0.99;

  beforeEach(async function() {
    order = await Order.new(customer, vendor, listing.price, listing.id);
  });

  it('should have state Created', async function() {
    let result = await order.state.call();
    assert.equal(states[result.toNumber()], 'Created');
  });

  it('should accept funding', async function() {
    await order.send(listing.price, {from: customer});
    let result = await order.getBalance.call();
    assert.equal(result.toNumber(), listing.price);
  });

  it('should not accept funding from vendor', async function() {
    assert(throwsError(async function () {
      await order.send(listing.price, {from: vendor});
    }));
  });

  it('should allow finalization from state Created', async function() {
    await order.finalize({from: customer});
    let result = await order.state.call();
    assert.equal(states[result.toNumber()], 'Finalized');
  });

  it('should not allow finalization from vendor', async function() {
    assert(throwsError(async function() {
      await order.finalize({from: vendor});
    }));
  });

  it('should allow shipping from state Created', async function() {
    await order.ship({from: vendor});
    let result = await order.shipped.call();
    assert(result);
  });

  it('should not allow shipping from customer', async function() {
    assert(throwsError(async function() {
      await order.ship({from: customer});
    }));
  });

  it('should allow withdrawals by customer in state Created', async function() {
    await order.send(listing.price, {from: customer});
    
    let prevBalance = balance.get(customer);
    await order.withdraw({from: customer});
    assert(balance.changed(prevBalance, balance.get(customer), listing.price, true));
  });


  it('should allow withdrawals by vendor in state Finalized', async function() {
    await order.send(listing.price, {from: customer});
    await order.finalize({from: customer});

    let prevBalance = balance.get(vendor);
    await order.withdraw({from: vendor});
    assert(balance.changed(prevBalance, balance.get(vendor), listing.price, true));
  });


});