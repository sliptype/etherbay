export default {

  get: function (address) {
    return web3.eth.getBalance(address);
  },

  changed: function (pre, post, expected, increase) {
    const threshold = 0.99;

    let difference = increase ? post.minus(pre) : pre.minus(post);

    let percentage = difference.dividedBy(expected);

    return percentage.greaterThan(threshold);
  }


}