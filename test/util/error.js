export default async function(f) {
  try {
    await f();
  } catch (e) {
    return(e instanceof Error);
  }
  return(false);
}