const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: ['babel-polyfill', './app/js/main.js'],
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'app.js'
  },
  devtool: 'source-map',
  plugins: [
    // Copy our app's index.html to the build folder.
    new CopyWebpackPlugin([
      { from: './app/index.html', to: "index.html", copyUnmodified: "true" }
    ])
  ],
  module: {
    rules: [
      {
       test: /\.scss$/,
       use: [ 'style-loader', 'css-loader', 'sass-loader' ]
      },
      {
       test: /\.css$/,
       use: [  'css-loader' ]
      },
      {
        test: /\.vue$/,
        use: [ 'vue-loader' ]
      }
    ],
    loaders: [
      { test: /\.json$/, use: 'json-loader' },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015'],
          plugins: ['transform-runtime']
        }
      }
    ]
  }
}
